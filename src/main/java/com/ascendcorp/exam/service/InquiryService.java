package com.ascendcorp.exam.service;

import com.ascendcorp.exam.exception.CustomException;
import com.ascendcorp.exam.model.InquiryServiceRequest;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.xml.ws.WebServiceException;

@Service
public class InquiryService {

    @Autowired
    private BankProxyGateway bankProxyGateway;

    private static final String RESPONSE_DESC_CODE_98 = "98";
    private static final String ERROR_MSG_GENERAL_INVALID_DATA = "General Invalid Data";

    final static Logger log = Logger.getLogger(InquiryService.class);

    public InquiryServiceResultDTO inquiry(InquiryServiceRequest requestObj
                                          )
    {
        InquiryServiceResultDTO respDTO = null;
        try
        {

            checkInputParams(requestObj);

            log.info("call bank web service");
            TransferResponse response = bankProxyGateway.requestTransfer(requestObj.getTransactionId(), requestObj.getTranDateTime(), requestObj.getChannel(),
                    requestObj.getBankCode(), requestObj.getBankNumber(), requestObj.getAmount(), requestObj.getReference1(), requestObj.getReference2());

            log.info("check bank response code");

            if(response == null)
                // no resport from bank
                throw new CustomException("Unable to inquiry from service.");

            log.debug("found response code");
            respDTO = new InquiryServiceResultDTO();

            respDTO.setRef_no1(response.getReferenceCode1());
            respDTO.setRef_no2(response.getReferenceCode2());
            respDTO.setAmount(response.getAmount());
            respDTO.setTranID(response.getBankTransactionID());

            validateResponse(response,respDTO);


        }catch(NullPointerException ne)
        {
            respDTO = new InquiryServiceResultDTO();
            setErrorData("500",ERROR_MSG_GENERAL_INVALID_DATA,respDTO);
        }catch(WebServiceException r)
        {
            // handle error from bank web service
            String faultString = r.getMessage();
            // bank general error
            respDTO = new InquiryServiceResultDTO();
            setErrorData("504","Internal Application Error",respDTO);
            if(faultString != null && (faultString.indexOf("java.net.SocketTimeoutException") > -1
                    || faultString.indexOf("Connection timed out") > -1 ))
            {
                // bank timeout
                setErrorData("503","Error timeout",respDTO);
            }
        }
        catch(Exception e)
        {
            log.error("inquiry exception", e);
            respDTO = new InquiryServiceResultDTO();
            setErrorData("504","Internal Application Error",respDTO);
        }
        return respDTO;
    }

    public void setErrorData(String reasonCode, String reasonDescription, InquiryServiceResultDTO respObj){
        respObj.setReasonCode(reasonCode);
        respObj.setReasonDesc(reasonDescription);

    }

    public void checkInputParams(InquiryServiceRequest request){

        log.info("validate request parameters.");
        if(request.getTransactionId() == null) {
            log.info("Transaction id is required!");
            throw new NullPointerException("Transaction id is required!");
        }
        if(request.getTranDateTime() == null) {
            log.info("Transaction DateTime is required!");
            throw new NullPointerException("Transaction DateTime is required!");
        }
        if(request.getChannel() == null) {
            log.info("Channel is required!");
            throw new NullPointerException("Channel is required!");
        }
        if(!StringUtils.isNotBlank(request.getBankCode())) {
            log.info("Bank Code is required!");
            throw new NullPointerException("Bank Code is required!");
        }
        if(!StringUtils.isNotBlank(request.getBankNumber())) {
            log.info("Bank Number is required!");
            throw new NullPointerException("Bank Number is required!");
        }
        if(request.getAmount() <= 0) {
            log.info("Amount must more than zero!");
            throw new NullPointerException("Amount must more than zero!");
        }

    }


    public void validateResponse(TransferResponse response, InquiryServiceResultDTO respDTO) throws CustomException {

        String responseCode = response.getResponseCode().toLowerCase();
        String replyDesc = response.getDescription();

        boolean isReplyDescLengthValid = (replyDesc != null && StringUtils.countMatches(replyDesc,":") >= 1);

        switch (responseCode) {
            case "approved":
                // bank response code = approved
                setErrorData("200",response.getDescription(),respDTO);
                respDTO.setAccountName(response.getDescription());
                break;
            case "invalid_data":
                // bank response code = invalid_data
                // bank description short format and bank no description
                setErrorData("400",ERROR_MSG_GENERAL_INVALID_DATA,respDTO);
                if(replyDesc != null && StringUtils.countMatches(replyDesc,":") >= 2)
                {
                    // bank description full format
                    String[] respDesc = replyDesc.split(":");
                    setErrorData(respDesc[1],respDesc[2],respDTO);

                }
                break;
            case "transaction_error":
                // bank response code = transaction_error
                // bank description incorrect format and bank no description
                setErrorData("500", "General Transaction Error",respDTO);

                if(isReplyDescLengthValid)
                {
                    String[] respDesc = replyDesc.split(":");
                    log.info("Case Inquiry Error Code Format Now Will Get From [0] and [1] first");
                    String subIdx1 = respDesc[0];
                    String subIdx2 = respDesc[1];
                    log.info("index[0] : "+subIdx1 + " index[1] is >> "+subIdx2);
                    // bank description short format and  bank code 98
                    setErrorData(subIdx1, subIdx2,respDTO);
                    if(respDesc.length >= 3 && !RESPONSE_DESC_CODE_98.equalsIgnoreCase(subIdx1)){
                        // bank description full format
                        String subIdx3 = respDesc[2];
                        log.info("index[0] : "+subIdx3);
                        setErrorData(subIdx2, subIdx3,respDTO);
                    }

                }
                break;
            case "unknown":
                // bank description short format and  bank no description
                setErrorData("501",ERROR_MSG_GENERAL_INVALID_DATA,respDTO);
                if(isReplyDescLengthValid)
                {
                    String[] respDesc = replyDesc.split(":");
                    String reasonCode = ( respDesc[1] == null ||  respDesc[1].trim().length() == 0) ? ERROR_MSG_GENERAL_INVALID_DATA : respDesc[1];
                    // bank description full format
                    setErrorData(respDesc[0],reasonCode,respDTO);
                }
                break;

            default:
                // bank code not support
                throw new CustomException("Unsupport Error Reason Code");

        }

    }


}
