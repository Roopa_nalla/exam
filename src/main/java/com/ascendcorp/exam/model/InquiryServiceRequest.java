package com.ascendcorp.exam.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class InquiryServiceRequest {

    private String transactionId;
    private Date tranDateTime;
    private String channel;
    private String bankCode;
    private String bankNumber;
    private double amount;
    private String reference1;
    private String reference2;

}
