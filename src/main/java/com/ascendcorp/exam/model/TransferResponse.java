package com.ascendcorp.exam.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransferResponse {


    private String responseCode;
    private String description;
    private String referenceCode1;
    private String referenceCode2;
    private String amount;
    private String bankTransactionID;

}
